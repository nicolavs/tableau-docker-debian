FROM jrei/systemd-debian:9

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y locales

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8

ENV LANG en_US.UTF-8

RUN apt-get install -y fontconfig fuse net-tools bash-completion gdb chrpath freeglut3 libcap2-bin \
    libegl1-mesa libfreetype6 libfuse2 libgssapi-krb5-2  libxcomposite1 libxcursor1 default-jre gdebi-core \
    libxi6 libxrandr2 libxrender1 libxslt1.1 libxtst6 lsb-core lsb-release sudo gettext-base \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /run/systemd/system

RUN touch /usr/local/bin/sysctl \
	&& chmod +x /usr/local/bin/sysctl

RUN echo 'seq 0 9 | xargs -I% -- echo %,%' \
	| tee /usr/local/bin/lscpu \
	&& chmod +x /usr/local/bin/lscpu
RUN echo cpu: $(lscpu -p | grep -E -v '^#' | sort -u -t, -k 2,4 | wc -l)

RUN echo 'echo; echo mem: 32768' \
	| tee /usr/local/bin/free \
	&& chmod +x /usr/local/bin/free
RUN echo mem: $(free -m | awk 'NR == 2 { print $2; }')

RUN echo "PATH=/usr/local/bin:$PATH" | tee -a /etc/environment

WORKDIR /data
# https://www.tableau.com/support/releases/server
ADD tableau-server-2020-4-0_amd64.deb ./tableau-server.deb

RUN gdebi -n tableau-server.deb

RUN ln -svf /opt/tableau/tableau_server/packages/scripts.*/initialize-tsm \
	/usr/local/bin

RUN find /opt/tableau/tableau_server/packages/customer-bin.* \
	-not -type d -exec ln -svf {} /usr/local/bin/ \;

RUN rm tableau-server.deb

ARG USER=tableau

RUN ( \
		echo "${USER} ALL=(ALL) NOPASSWD:ALL" ; \
		echo "Defaults:${USER} !requiretty"   ; \
		echo "Defaults secure_path = $PATH"   ; \
	) | tee -a /etc/sudoers
RUN useradd -g users -m "${USER}"
RUN usermod -aG sudo "${USER}"

RUN echo '[Unit]\n\
Description=/etc/rc.local\n\
ConditionPathExists=/etc/rc.local\n\
\n\
[Service]\n\
Type=forking\n\
ExecStart=/etc/rc.local start\n\
TimeoutSec=0\n\
StandardOutput=tty\n\
RemainAfterExit=yes\n\
SysVStartPriority=99\n\
\n\
[Install]\n\
WantedBy=multi-user.target' \
> /etc/systemd/system/rc-local.service

RUN ( \
		echo "#!/bin/sh -e\n\
		runuser -l '${USER}' -c 'sudo $(realpath $(which initialize-tsm)) --accepteula'"; \
	) | tee -a /etc/rc.local

RUN echo "${USER}:${USER}" | chpasswd
RUN systemctl enable rc-local || chmod +x /etc/rc.local

EXPOSE 80 8850

# docker run --rm -itd -p 8850:8850 --stop-signal=SIGRTMIN+3  --privileged --security-opt=seccomp:unconfined -v /sys/fs/cgroup:/sys/fs/cgroup:ro --name tableau_deb_1 tableau-docker-debian
